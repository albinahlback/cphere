/* Copyright (C) 2021 Albin Ahlbäck
 *
 * This file is part of Cphere.
 *
 * Cphere is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (LGPL) as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.  See <https://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <unistd.h>
#include "SDL2/SDL.h"
#include "../color.h"
#include "../sdl.h"

int
main(int argc, char* argv[])
{
    if (argc != 3)
    {
        printf("Wrong usage.\n\n");
        printf("Usage:\n");
        printf("    file.out WIDTH HEIGHT\n");
        return 1;
    }

    int const width = strtol(argv[1], NULL, 10);
    int const height = strtol(argv[2], NULL, 10);

    SDL_Renderer *renderer;
    SDL_Window *window;
    SDL_Event event;
    int quit = 0;

    SDL_Rect rect;

    color_t bg, scale[2];

    rect.x = width / 2;
    rect.y = height / 2;
    rect.w = 50;
    rect.h = 100;

    color_set(&bg, 255, 255, 255, 255);
    color_set(&scale[0], 125, 47, 82, 189);
    color_set(&scale[1], 15, 47, 182, 255);

    /* Initialize SDL */
    SDL_Init(SDL_INIT_VIDEO);
    window = SDL_CreateWindow(NULL,
                SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                width, height,
                SDL_WINDOW_OPENGL);
    renderer = SDL_CreateRenderer(window, -1, 0);

    while (!quit)
    {
        SDL_WaitEvent(&event);
        switch (event.type)
        {
            case SDL_QUIT:
                quit = 1;
                break;
            case SDL_KEYDOWN:
                SDL_set_render_color(renderer, &bg);
                SDL_RenderClear(renderer);
                switch (event.key.keysym.sym){
                    case SDLK_j:
                        SDL_set_render_color(renderer, &scale[0]);
                        SDL_RenderDrawRect(renderer, &rect);
                        break;
                    case SDLK_k:
                        SDL_set_render_color(renderer, &scale[1]);
                        SDL_RenderDrawRect(renderer, &rect);
                        break;
                }
        }
        SDL_RenderPresent(renderer);
    }

    /* Clear SDL */
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}

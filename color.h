/* Copyright (C) 2021 Albin Ahlbäck
 *
 * This file is part of Cphere.
 *
 * Cphere is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (LGPL) as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.  See <https://www.gnu.org/licenses/>. */

#ifndef COLOR_H
#define COLOR_H

struct color
{
    char r;
    char b;
    char g;
    char a;
};
typedef struct color color_t;

void color_set(color_t *clr, char r, char b, char g, char a);
void color_set_str(color_t *clr, char str[]);

#endif

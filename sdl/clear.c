/* Copyright (C) 2021 Albin Ahlbäck
 *
 * This file is part of Cphere.
 *
 * Cphere is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (LGPL) as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.  See <https://www.gnu.org/licenses/>. */

#include "SDL2/SDL.h"

void
SDL_clear(SDL_Window **window, SDL_Renderer **renderer)
{
    SDL_DestroyRenderer(*renderer);
    SDL_DestroyWindow(*window);
    SDL_Quit();
}

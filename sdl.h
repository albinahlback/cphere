/* Copyright (C) 2021 Albin Ahlbäck
 *
 * This file is part of Cphere.
 *
 * Cphere is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (LGPL) as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.  See <https://www.gnu.org/licenses/>. */

#ifndef SDL_H
#define SDL_H

#include "SDL2/SDL.h"
#include "color.h"

void SDL_set_render_color(SDL_Renderer *renderer, color_t *color);

/* FIXME */
void SDL_initialize(SDL_Window **window, SDL_Renderer **renderer,
                                                int width, int height);

/* FIXME */
void SDL_clear(SDL_Window **window, SDL_Renderer **renderer);

#endif
